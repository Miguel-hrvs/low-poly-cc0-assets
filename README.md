Low poly CC0 assets by Miguel

Suitable for any low poly and low resource game with a 90's aesthetic, you can tweak them in blockbench with the bbmodel file.

Now I'm not taking tips, maybe in the future if the list is big enough and helps anybody.

These assets are ideas for my godot projects so I'll be posting here almost weakly.

If you are making a similar project contact with me and I will list it here to help people finding more assets.

## Author
Miguel Hervás

## License
CC0 - Public domain
